//
//  UIImage+Extension.swift
//  Tog
//
//  Created by Marco Margarucci on 26/09/21.
//
//  Converts an UIImage to CKAsset


import Foundation
import UIKit
import CloudKit

extension UIImage {
    // Converts to CKAsset
    func convertToCKAsset() -> CKAsset? {
        // Get base document directory URL
        guard let urlPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        // Append a unique identifier to profile image
        let fileURL = urlPath.appendingPathComponent("selectedAvatarImage")
        // Write the image data to the location the address points to
        guard let imageData = jpegData(compressionQuality: 0.25) else { return nil }
        // Create CKAsset with the fileURL
        do {
            try imageData.write(to: fileURL)
            return CKAsset(fileURL: fileURL)
        } catch {
            return nil
        }
    }
}
