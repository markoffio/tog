//
//  HapticManager.swift
//  Tog
//
//  Created by Marco Margarucci on 28/10/21.
//

import Foundation
import UIKit

struct HapticManager {
    static func playSuccess() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
}
