//
//  LocationManager.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation

final class LocationManager: ObservableObject {
    // MARK: - Properties
    // Array of locations
    @Published var locations: [TogLocation] = []
    // Selected location
    var selectedLocation: TogLocation?
}
