//
//  CloudKitManager.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import CloudKit

final class CloudKitManager {
            
    // MARK: - Properties
    
    static let shared = CloudKitManager()
    
    // User record
    var userRecord: CKRecord?
    // Profile record ID
    var profileRecordID: CKRecord.ID?
    
    // MARK: - Functions
    
    // Initializer
    private init() {}
    
    // Get user record
    func getUserRecord() {
        // Get UserRecordID from the container
        CKContainer.default().fetchUserRecordID { recordID, error in
            guard let recordID = recordID, error == nil else {
                debugPrint("[ERROR]: \(error!.localizedDescription)")
                return
            }
            
            // Get UserRecord from the public database
            CKContainer.default().publicCloudDatabase.fetch(withRecordID: recordID) { userRecord, error in
                guard let userRecord = userRecord, error == nil else {
                    debugPrint("[ERROR]: \(error!.localizedDescription)")
                    return
                }
                
                self.userRecord = userRecord
                // Check if the user has a profile on CloudKit
                if let profileReference = userRecord["userProfile"] as? CKRecord.Reference {
                    self.profileRecordID = profileReference.recordID
                }
            }
        }
    }
    
    // Get locations from cloud default database
    func getLocations(completed: @escaping(Result<[TogLocation], Error>) -> Void) {
        // Used to get locations sorted by name
        let sortDescriptor = NSSortDescriptor(key: TogLocation.kName, ascending: true)
        let query = CKQuery(recordType: RecordType.location, predicate: NSPredicate(value: true))
        query.sortDescriptors = [sortDescriptor]
        // Access the public cloud database
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) { records, error in
            guard let records = records, error == nil else {
                completed(.failure(error!))
                return
            }
            let locations = records.map(TogLocation.init)
            completed(.success(locations))
        }
    }
    
    // Save records to cloud database
    func batchSave(records: [CKRecord], completed: @escaping (Result<[CKRecord], Error>) -> Void) {
        // Create CKOperation to save user and profile record
        let operation = CKModifyRecordsOperation(recordsToSave: records)
        // IMPORTANT: modifyRecordsCompletionBlock IS DEPRECATED IN iOS 15: modifyRecordsResultBlock
        operation.modifyRecordsCompletionBlock = { savedRecords, _, error in
            guard let savedRecords = savedRecords, error == nil else {
                completed(.failure(error!))
                return
            }
            completed(.success(savedRecords))
        }
        CKContainer.default().publicCloudDatabase.add(operation)
    }
    
    // Get all users checked in a specific location
    func getCheckedInProfiles(for locationID: CKRecord.ID, completed: @escaping (Result<[TogProfile], Error>) -> Void) {
        let reference = CKRecord.Reference(recordID: locationID, action: .none)
        let predicate = NSPredicate(format: "isCheckedIn == %@", reference)
        let query = CKQuery(recordType: RecordType.profile, predicate: predicate)
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {records, error in
            guard let records = records, error == nil else {
                completed(.failure(error!))
                return
            }
            let profiles = records.map(TogProfile.init)
            completed(.success(profiles))
        }
    }
    
    // Get checked in profiles as dictionary
    func getCheckedInProfilesDictionary(completed: @escaping (Result<[CKRecord.ID: [TogProfile]], Error>) -> Void) {
        let predicate = NSPredicate(format: "isCheckedInNilCheck == 1")
        let query = CKQuery(recordType: RecordType.profile, predicate: predicate)
        let operation = CKQueryOperation(query: query)
        var checkedInProfiles: [CKRecord.ID: [TogProfile]] = [:]
        // IMPORTANT: recordFetchedBlock IS DEPRECATED IN iOS 15: recordMatchedBlock
        operation.recordFetchedBlock = { record in
            let profile = TogProfile(record: record)
            guard let locationReference = record[TogProfile.kIsCheckedIn] as? CKRecord.Reference else { return }
            checkedInProfiles[locationReference.recordID, default: []].append(profile)
        }
        // IMPORTANT: queryCompletionBlock IS DEPRECATED IN iOS 15: queryResultBlock
        operation.queryCompletionBlock = { cursor, error in
            guard error == nil else {
                completed(.failure(error!))
                return
            }
            if let cursor = cursor {
                self.continueWithCheckedInProfilesDictionary(cursor: cursor, dictionary: checkedInProfiles) { result in
                    switch result {
                    case .success(let profiles):
                        completed(.success(profiles))
                    case .failure(let error):
                        completed(.failure(error))
                    }
                }
            } else {
                completed(.success(checkedInProfiles))
            }
        }
        CKContainer.default().publicCloudDatabase.add(operation)
    }
    
    func continueWithCheckedInProfilesDictionary(cursor: CKQueryOperation.Cursor, dictionary: [CKRecord.ID: [TogProfile]], completed: @escaping (Result<[CKRecord.ID: [TogProfile]], Error>) -> Void) {
        var checkedInProfiles = dictionary
        let operation = CKQueryOperation(cursor: cursor)
        // IMPORTANT: recordFetchedBlock IS DEPRECATED IN iOS 15: recordMatchedBlock
        operation.recordFetchedBlock = { record in
            let profile = TogProfile(record: record)
            guard let locationReference = record[TogProfile.kIsCheckedIn] as? CKRecord.Reference else { return }
            checkedInProfiles[locationReference.recordID, default: []].append(profile)
        }
        // IMPORTANT: queryCompletionBlock IS DEPRECATED IN iOS 15: queryResultBlock
        operation.queryCompletionBlock = { cursor, error in
            guard error == nil else {
                completed(.failure(error!))
                return
            }
            if let cursor = cursor {
                self.continueWithCheckedInProfilesDictionary(cursor: cursor, dictionary: checkedInProfiles) { result in
                    switch result {
                    case .success(let profiles):
                        completed(.success(profiles))
                    case .failure(let error):
                        completed(.failure(error))
                    }
                }
            } else {
                completed(.success(checkedInProfiles))
            }
        }
        CKContainer.default().publicCloudDatabase.add(operation)
    }
    
    // Get the number of checked in profiles in a specific location
    func getCheckedInProfilesCount(completed: @escaping (Result<[CKRecord.ID: Int], Error>) -> Void) {
        let predicate = NSPredicate(format: "isCheckedInNilCheck == 1")
        let query = CKQuery(recordType: RecordType.profile, predicate: predicate)
        let operation = CKQueryOperation(query: query)
        operation.desiredKeys = [TogProfile.kIsCheckedIn]
        var checkedInProfiles: [CKRecord.ID: Int] = [:]
        // IMPORTANT: recordFetchedBlock IS DEPRECATED IN iOS 15: recordMatchedBlock
        operation.recordFetchedBlock = { record in
            guard let locationReference = record[TogProfile.kIsCheckedIn] as? CKRecord.Reference else { return }
            if let count = checkedInProfiles[locationReference.recordID] {
                checkedInProfiles[locationReference.recordID] = count + 1
            } else {
                checkedInProfiles[locationReference.recordID] = 1
            }
        }
        // IMPORTANT: queryCompletionBlock IS DEPRECATED IN iOS 15: queryResultBlock
        operation.queryCompletionBlock = { cursor, error in
            guard error == nil else {
                completed(.failure(error!))
                return
            }
            completed(.success(checkedInProfiles))
        }
        CKContainer.default().publicCloudDatabase.add(operation)
    }
    
    // Save profile
    func save(record: CKRecord, completed: @escaping (Result<CKRecord, Error>) -> Void) {
        CKContainer.default().publicCloudDatabase.save(record) { record, error in
            guard let record = record, error == nil else {
                completed(.failure(error!))
                return
            }
            completed(.success(record))
        }
    }
    
    // Fetch a record with a specific identifier
    func fetchRecord(with id: CKRecord.ID, completed: @escaping (Result<CKRecord, Error>) -> Void) {
        CKContainer.default().publicCloudDatabase.fetch(withRecordID: id) { record, error in
            guard let record = record, error == nil else {
                completed(.failure(error!))
                return
            }
            completed(.success(record))
        }
    }
}
