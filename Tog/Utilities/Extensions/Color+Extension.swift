//
//  Color+Extension.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import Foundation
import SwiftUI

extension Color {
    // Brand primary color
    static let brandPrimary = Color("brandPrimary")
    // Brand secondary color
    static let brandSecondary = Color("brandSecondary")
    // Controls primary
    static let controlsPrimary = Color("controlsPrimary")
    // Controls secondary
    static let controlsSecondary = Color("controlsSecondary")
    // Dark blue
    static let darkBlue = Color("darkBlue")
    // Map marker
    static let mapMarker = Color("mapMarker")
    // Map annotation
    static let mapAnnotation = Color("mapAnnotation")
    // Dark gray
    static let darkGray = Color("darkGray")
}
