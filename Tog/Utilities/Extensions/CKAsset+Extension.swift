//
//  CKAsset+Extension.swift
//  Tog
//
//  Created by Marco Margarucci on 25/09/21.
//

import Foundation
import CloudKit
import UIKit

extension CKAsset {
    // Convert to UIImage: get file URL image data an then converts it to UIImage. Return an image placeholder if can't get data
    func convertToUIImage(in dimension: ImageDimension) -> UIImage {
        // Get file URL
        guard let fileURL = self.fileURL else { return dimension.placeholder }
        do {
            // Get data from file URL
            let data = try Data(contentsOf: fileURL)
            return UIImage(data: data) ?? dimension.placeholder
        } catch {
            return dimension.placeholder
        }
    }
}
