//
//  View+Extension.swift
//  Tog
//
//  Created by Marco Margarucci on 24/08/21.
//

import SwiftUI

extension View {
    func profileNameStyle() -> some View {
        self.modifier(ProfileNameText())
    }
    
    // Dismiss keyboard
    func dismissKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
        
    // Embed in scroll view
    func embedInScrollView(axis: Axis.Set, showsIndicators: Bool) -> some View {
        GeometryReader { geometry in
            ScrollView(axis, showsIndicators: showsIndicators) {
                frame(minHeight: geometry.size.height , maxHeight: .infinity)
            }
        }
    }
}
