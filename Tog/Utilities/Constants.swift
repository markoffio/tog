//
//  Constants.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import UIKit

// Record type
enum RecordType {
    // Location
    static let location: String = "TogLocation"
    // Profile
    static let profile: String = "TogProfile"
}

// Placeholder image
enum PlaceholderImage {
    // Avatar
    static let avatar = UIImage(named: "profile")!
    // Square
    static let square = UIImage(named: "android")!
    // Banner
    static let banner = UIImage(named: "restaurant")!
}

// Image dimension
enum ImageDimension {
    case square, banner
    
    var placeholder: UIImage {
        switch self {
        case .square:
            return PlaceholderImage.square
        case .banner:
            return PlaceholderImage.banner
        }
    }
}

// Onboarding view
enum OnboardingViewShow {
    static let kHasSeenOnboardingView: String = "hasSeenOnboardingView"
}

// Device types
enum DeviceTypes {
    // Screen size
    enum ScreenSize {
        static let width = UIScreen.main.bounds.size.width
        static let height = UIScreen.main.bounds.size.height
        static let maxLength = max(ScreenSize.width, ScreenSize.height)
        static let minLength = min(ScreenSize.width, ScreenSize.height)
    }
    
    static let idiom = UIDevice.current.userInterfaceIdiom
    static let nativeScale = UIScreen.main.nativeScale
    static let scale = UIScreen.main.scale
    
    static let isiPhoneSE = idiom == .phone && ScreenSize.maxLength == 568.0
    static let isiPhone8Standard = idiom == .phone && ScreenSize.maxLength == 667.0 && nativeScale == scale
    static let isiPhone8Zoomed = idiom == .phone && ScreenSize.maxLength == 667.0 && nativeScale > scale
    static let isiPhone8PlusStandard = idiom == .phone && ScreenSize.maxLength == 736.0
    static let isiPhone8PlusZoomed = idiom == .phone && ScreenSize.maxLength == 736.0 && nativeScale > scale
    static let isiPhoneX = idiom == .phone && ScreenSize.maxLength == 812.0
    static let isiPhoneXsMaxAndXr = idiom == .phone && ScreenSize.maxLength == 896.0
    static let isiPad = idiom == .pad && ScreenSize.maxLength >= 1024.0
    
    static func isiPhoneXAspectRation() -> Bool { return isiPhoneX || isiPhoneXsMaxAndXr }
}
