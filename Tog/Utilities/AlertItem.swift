//
//  AlertItem.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
    
    var alert: Alert {
        Alert(title: title, message: message, dismissButton: dismissButton)
    }
}

struct AlertContext {
    // MARK: - MapView Errors
    
    // Unable to get locations
    static let unableToGetLocations = AlertItem(title: Text("Errore accesso località"), message: Text("Impossibile visualizzare le località.\nProva in un altro momento."), dismissButton: .default(Text("OK")))
    
    // MARK: - Device location errors
    
    // Unable to get device location
    static let locationRestricted = AlertItem(title: Text("Accesso posizione limitato"), message: Text("L'accesso alla tua posizione è limitato"), dismissButton: .default(Text("OK")))
    
    // Location denied
    static let locationDenied = AlertItem(title: Text("Accesso posizione negato"), message: Text("Tog non ha il permesso di accedere alla tua posizione. Accedi alle impostazioni del tuo telefono per modificare le preferenze."), dismissButton: .default(Text("OK")))
    
    // Location disabled
    static let locationDisabled = AlertItem(title: Text("Accesso posizione disabilitato"), message: Text("L'accesso alla tua posizione non è abilitato. TAccedi alle impostazioni del tuo telefono per modificare le preferenze."), dismissButton: .default(Text("OK")))
    
    // Checked in count
    static let checkedInCount = AlertItem(title: Text("Errore di rete"), message: Text("Impossibile controllare il numero di persone che hanno effettuato il checkin. Controlla la tua connessione internet."), dismissButton: .default(Text("OK")))
    
    // MARK: - Profile view errors
    
    // Invalid profile
    static let invalidProfile = AlertItem(title: Text("Profilo non valido"), message: Text("E' necessario specificare tutte le informazioni richieste per completare il profilo. Per favore, prova di nuovo."), dismissButton: .default(Text("OK")))
    
    // No user records
    static let noUserRecord = AlertItem(title: Text("Nessun utente trovato"), message: Text("Effettua l'accesso ad iCloud nella sezione \"Impostazioni\" del tuo telefono."), dismissButton: .default(Text("OK")))
    
    // User profile created successfully
    static let createProfileSuccess = AlertItem(title: Text("Profilo creato con successo"), message: Text("Il tuo profilo è stato creato con successo."), dismissButton: .default(Text("OK")))
    
    // User profile creation failure
    static let createProfileFailure = AlertItem(title: Text("Errore creazione profilo"), message: Text("Si è verificato un errore nella creazione del tuo profilo. Riprova più tardi."), dismissButton: .default(Text("OK")))
    
    // Unable to get profile
    static let unableProfileFailure = AlertItem(title: Text("Impossibile recuperare il profilo"), message: Text("Si è verificato un errore nel recupero delle informazioni del tuo profilo. Riprova più tardi."), dismissButton: .default(Text("OK")))
    
    // Unable to update profile
    static let updateProfileFailure = AlertItem(title: Text("Impossibile aggiornare il profilo"), message: Text("Si è verificato un errore durante l'aggiornamento del tuo profilo. Riprova più tardi."), dismissButton: .default(Text("OK")))
    
    // User profile updated successfully
    static let updateProfileSuccess = AlertItem(title: Text("Profilo aggiornato con successo"), message: Text("Il tuo profilo è stato aggiornato con successo."), dismissButton: .default(Text("OK")))
    
    // Unable to get profile
    static let unableToGetProfile = AlertItem(title: Text("Impossibile recuperare le informazioni del profilo"), message: Text("Non è stato possibile recuperare le informazioni del profilo. Provare più tardi."), dismissButton: .default(Text("OK")))
    
    // MARK: - Location details view errors
    
    // Invalid phone number
    static let invalidPhoneNumber = AlertItem(title: Text("Numero di telefono non valido"), message: Text("Il numero di telefono fornito non è valido."), dismissButton: .default(Text("OK")))
    
    // Unable to get check in status
    static let unableToGetCheckInStatus = AlertItem(title: Text("Impossibile controllare lo stato"), message: Text("Non è stato possibile controllare lo stato dell'utente."), dismissButton: .default(Text("OK")))
    
    // Unable to check in or out
    static let unableToCheckInOrOut = AlertItem(title: Text("Errore"), message: Text("Non è stato possibile effettuare il check in/out. Provare più tardi."), dismissButton: .default(Text("OK")))
    
    // Unable to get checked in profiles
    static let unableToGetCheckedInProfiles = AlertItem(title: Text("Errore"), message: Text("Non è stato possibile visualizzare gli utenti che hanno effettuato il check in in questo locale. Provare più tardi."), dismissButton: .default(Text("OK")))
    
    // MARK: - Location list view errors
    
    // Unable to get checked in profiles
    static let unableToGetAllCheckedInProfiles = AlertItem(title: Text("Errore"), message: Text("Non è stato possibile visualizzare gli utenti che hanno effettuato il check in in questo locale. Provare più tardi."), dismissButton: .default(Text("OK")))
}
