//
//  TogProfile.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import CloudKit
import UIKit

struct TogProfile: Identifiable {
    // Constants
    static let kFirstName: String = "firstName"
    static let kLastName: String = "lastName"
    static let kAvatar: String = "avatar"
    static let kCompanyName: String = "companyName"
    static let kBio: String = "bio"
    static let kIsCheckedIn: String = "isCheckedIn"
    static let kIsCheckedInNilCheck: String = "isCheckedInNilCheck"
    
    let id: CKRecord.ID
    let name: String
    let lastName: String
    let avatar: CKAsset!
    let companyName: String
    let bio: String
    
    // Create avatar image
    var avatarImage: UIImage {
        guard let avatar = avatar else { return PlaceholderImage.avatar }
        return avatar.convertToUIImage(in: .square)
    }
    
    // Initializer
    init(record: CKRecord) {
        id = record.recordID
        name = record[TogProfile.kFirstName] as? String ?? "N/A"
        lastName = record[TogProfile.kLastName] as? String ?? "N/A"
        avatar = record[TogProfile.kAvatar] as? CKAsset
        companyName = record[TogProfile.kCompanyName] as? String ?? "N/A"
        bio = record[TogProfile.kBio] as? String ?? "N/A"
    }
}
