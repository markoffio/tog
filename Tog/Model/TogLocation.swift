//
//  TogLocation.swift
//  Tog
//
//  Created by Marco Margarucci on 22/09/21.
//

import Foundation
import CloudKit
import UIKit

struct TogLocation: Identifiable {    
    // Constants
    static let kName: String = "name"
    static let kDescription: String = "description"
    static let kSquareAsset: String = "squareAsset"
    static let kBannerAsset: String = "bannerAsset"
    static let kAddress: String = "address"
    static let kLocation: String = "location"
    static let kWebsiteURL: String = "websiteURL"
    static let kPhoneNumber: String = "phoneNumber"
    
    let id: CKRecord.ID
    let name: String
    let description: String
    let squareAsset: CKAsset!
    let bannerAsset: CKAsset!
    let address: String
    let location: CLLocation
    let websiteURL: String
    let phoneNumber: String
    
    // Creare square image
    var squareImage: UIImage {
        guard let asset = squareAsset else { return PlaceholderImage.square }
        return asset.convertToUIImage(in: .square)
    }
    
    // Create banner image
    var bannerImage: UIImage {
        guard let asset = bannerAsset else { return PlaceholderImage.banner }
        return asset.convertToUIImage(in: .banner)
    }
    
    // Initializer
    init(record: CKRecord) {
        id = record.recordID
        name = record[TogLocation.kName] as? String ?? "N/A"
        description = record[TogLocation.kDescription] as? String ?? "N/A"
        squareAsset = record[TogLocation.kSquareAsset] as? CKAsset
        bannerAsset = record[TogLocation.kBannerAsset] as? CKAsset
        address = record[TogLocation.kAddress] as? String ?? "N/A"
        location = record[TogLocation.kLocation] as? CLLocation ?? CLLocation(latitude: 0, longitude: 0)
        websiteURL = record[TogLocation.kWebsiteURL] as? String ?? "N/A"
        phoneNumber = record[TogLocation.kPhoneNumber] as? String ?? "N/A"
    }
}
