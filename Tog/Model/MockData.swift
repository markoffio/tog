//
//  MockData.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import CloudKit

struct MockData {
    // Location
    static var location: CKRecord {
        let record = CKRecord(recordType: RecordType.location)
        record[TogLocation.kName] = "Pioppi Caffè"
        record[TogLocation.kAddress] = "Via Caracciolo, Pioppi"
        record[TogLocation.kDescription] = "Bar e ristorante a Pioppi - Cilento"
        record[TogLocation.kWebsiteURL] = "https://www.facebook.com/PioppiCaffeRistorante/"
        record[TogLocation.kLocation] = CLLocation(latitude: 40.1744362, longitude: 15.0886271)
        record[TogLocation.kPhoneNumber] = "+3999999999"
        return record
    }
    
    // Storie di Pane
    static var storieDiPane: CKRecord {
        let record = CKRecord(recordType: RecordType.location, recordID: CKRecord.ID(recordName: "1AE4096C-62E7-92EC-B5B3-8466B1240597"))
        record[TogLocation.kName] = "Storie di Pane"
        record[TogLocation.kAddress] = "Via A. Rubino, 1 84078 Vallo della Lucania (SA)"
        record[TogLocation.kDescription] = "Pane, panini, focacce, pizza, pasta fresca, salumi tipici cilentani, aperitivo, caffè, piatti caldi a Vallo della Lucania e Capaccio, Cilento"
        record[TogLocation.kWebsiteURL] = "http://www.storiedipane.com/"
        record[TogLocation.kLocation] = CLLocation(latitude: 40.2282901, longitude: 15.2626225)
        record[TogLocation.kPhoneNumber] = "09741870716"
        return record
    }
    
    // Profile
    static var profile: CKRecord {
        let record = CKRecord(recordType: RecordType.profile)
        record[TogProfile.kFirstName] = "Gaetano"
        record[TogProfile.kLastName] = "Bombarzetti"
        record[TogProfile.kCompanyName] = "Salicuneta Inc."
        record[TogProfile.kBio] = "This is my biography"
        return record
    }
}
