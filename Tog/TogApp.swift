//
//  TogApp.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI

@main
struct TogApp: App {
    let locationManager = LocationManager()
    var body: some Scene {
        WindowGroup {
            AppTabView().environmentObject(locationManager)
        }
    }
}
