//
//  LocationMapView.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI
import MapKit

struct LocationMapView: View {
    // MARK: - Properties
    
    // View model
    @StateObject private var viewModel = LocationMapViewModel()
    // Location manager
    @EnvironmentObject private var locationManager: LocationManager
    // Size category
    @Environment(\.sizeCategory) var sizeCategory
    
    var body: some View {
        VStack {
            // Map
            Map(coordinateRegion: $viewModel.region, showsUserLocation: true, annotationItems: locationManager.locations) { location in
                MapAnnotation(coordinate: location.location.coordinate, anchorPoint: CGPoint(x: 0.5, y: 0.75)) {
                    // Show custom annotation on the map
                    CustomAnnotation(location: location, number: viewModel.checkedInProfiles[location.id, default: 0])
                        .accessibilityLabel(Text(viewModel.createVoiceOverSummary(for: location)))
                        .onTapGesture {
                            locationManager.selectedLocation = location
                            viewModel.isShowingDetailView = true
                        }
                }
            }
            .accentColor(.mapMarker)
            .edgesIgnoringSafeArea(.top)
        }
        .sheet(isPresented: $viewModel.isShowingDetailView) {
            NavigationView {
                viewModel.createLocationDetailView(for: locationManager.selectedLocation!, in: sizeCategory)
                    .toolbar {
                        Button {
                            viewModel.isShowingDetailView = false
                        } label: {
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.mapMarker)
                                .frame(width: 25)
                        }

                    }
            }
        }
        // IMPORTANT: THIS ALERT IS DEPRECATED IN iOS 15
        .alert(item: $viewModel.alertItem, content: { $0.alert })
        .onAppear {
            if locationManager.locations.isEmpty {
                viewModel.getLocations(for: locationManager)
            }
            viewModel.getCheckedInCounts()
        }
    }
}

struct LocationMapView_Previews: PreviewProvider {
    static var previews: some View {
        LocationMapView().environmentObject(LocationManager())
    }
}
