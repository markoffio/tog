//
//  LocationMapViewModel.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import SwiftUI
import MapKit
import CloudKit

extension LocationMapView {
    
    final class LocationMapViewModel: ObservableObject {
        // MARK: - Properties
        // Alert item
         @Published var alertItem: AlertItem?
        // Is showing detail view
        @Published var isShowingDetailView: Bool = false
        // Region
        @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 40.17428335370894, longitude: 15.087783246143454), span: MKCoordinateSpan(latitudeDelta: 0.8, longitudeDelta: 0.8))
        // Checked in profiles
        @Published var checkedInProfiles: [CKRecord.ID: Int] = [:]

        
        // Get locations
        func getLocations(for locationManager: LocationManager) {
            CloudKitManager.shared.getLocations { [self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let locations):
                        locationManager.locations = locations
                    case .failure(_):
                        alertItem = AlertContext.unableToGetLocations
                    }
                }
            }
        }
        
        // Get checked in profiles count
        func getCheckedInCounts() {
            CloudKitManager.shared.getCheckedInProfilesCount { result in
                DispatchQueue.main.async { [self] in
                    switch result {
                    case .success(let checkedInProfiles):
                        self.checkedInProfiles = checkedInProfiles
                    case .failure(_):
                        alertItem = AlertContext.checkedInCount
                        break
                    }
                }
            }
        }
            
        // Create voice over summary
        func createVoiceOverSummary(for location: TogLocation) -> String {
            let count = checkedInProfiles[location.id, default: 0]
            let personPlurality = count == 1 ? "person" : "people"
            return "\(location.name) \(count) \(personPlurality) checkedin"
        }
        
        // Create location detail view based on different content size category
        @ViewBuilder func createLocationDetailView(for location: TogLocation, in sizeCategory: ContentSizeCategory) -> some View {
            if sizeCategory >= .accessibilityMedium {
                LocationDetailView(viewModel: LocationDetailView.LocationDetailViewModel(location: location)).embedInScrollView(axis: .vertical, showsIndicators: false)
            } else {
                LocationDetailView(viewModel: LocationDetailView.LocationDetailViewModel(location: location))
            }
        }
    }
}
