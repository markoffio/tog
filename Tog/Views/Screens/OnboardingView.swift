//
//  OnboardingView.swift
//  Tog
//
//  Created by Marco Margarucci on 25/09/21.
//

import SwiftUI

struct OnboardingView: View {
    // MARK: - Properties
    
    // Is showing onboarding view
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .foregroundColor(.mapMarker)
                        .frame(width: 25)
                }
            }
            .padding(.top, 30)
            .padding(.trailing, 30)
            Spacer()
            VStack {
                LogoView(frameWidth: 100)
                VStack(alignment: .leading, spacing: 30) {
                    OnboardingViewRow(imageName: "map", title: "Ristoranti", description: "Trova i luoghi vicino a te")
                    OnboardingViewRow(imageName: "checkmark.circle", title: "Arrivo", description: "Condividi la tua posizione")
                    OnboardingViewRow(imageName: "figure.wave.circle", title: "I tuoi amici", description: "Vedi dove sono i tuoi amici")
                }
                .padding(.top, 40)
            }
            .padding(.bottom, 120)
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}

fileprivate struct OnboardingViewRow: View {
    // MARK: - Properties
    
    // Image name
    var imageName: String
    // Title
    var title: String
    // Description
    var description: String
    
    var body: some View {
        HStack(spacing: 10) {
            Image(systemName: imageName)
                .resizable()
                .scaledToFit()
                .foregroundColor(Color.darkGray)
                .frame(width: 50)
            VStack(alignment: .leading, spacing: 10) {
                Text(title)
                    .bold()
                    .foregroundColor(.darkGray)
                Text(description)
                    .foregroundColor(Color.gray)
            }
            .padding(.leading, 20)
        }
        .padding([.leading, .trailing], 30)
    }
}
