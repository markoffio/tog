//
//  ProfileModalView.swift
//  Tog
//
//  Created by Marco Margarucci on 30/09/21.
//

import SwiftUI

struct ProfileModalView: View {
    // MARK: - Properties
    
    // User profile
    var profile: TogProfile
    
    @Binding var isShowingProfileModal: Bool
    
    var body: some View {
        ZStack {
            VStack {
                Spacer()
                    .frame(height: 60)
                // First name and last name
                Text(profile.name + " " + profile.lastName)
                    .bold()
                    .font(.title2)
                    .lineLimit(1)
                    .minimumScaleFactor(0.75)
                    .padding(.horizontal)
                // Company name
                Text(profile.companyName)
                    .fontWeight(.semibold)
                    .lineLimit(1)
                    .minimumScaleFactor(0.75)
                    .foregroundColor(.secondary)
                    .accessibilityLabel(Text("Works at \(profile.companyName)"))
                    .padding(.horizontal)
                // Biography
                Text(profile.bio)
                    .lineLimit(3)
                    .padding()
                    .accessibilityLabel(Text("Bio, \(profile.bio)"))
                    
            }
            .frame(width: 300, height: 230)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(16)
            .overlay(Button {
                withAnimation { isShowingProfileModal = false }
            } label: {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .foregroundColor(.mapMarker)
                    .frame(width: 25)
                    .padding([.top, .trailing], 10)
            }, alignment: .topTrailing)
            // Avatar image
            Image(uiImage: profile.avatarImage)
                .resizable()
                .scaledToFill()
                .frame(width: 110, height: 110)
                .clipShape(Circle())
                .shadow(color: .black.opacity(0.5), radius: 4, x: 0, y: 6)
                .offset(y: -125)
                .accessibilityHidden(true)
        }
        .accessibilityAddTraits(.isModal)
        .transition(.opacity.combined(with: .slide))
        .animation(.easeOut)
        .zIndex(2)
    }
}

/*
struct ProfileModalView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileModalView(profile: TogProfile(record: MockData.profile), isShowingProfileModal: true)
    }
}
*/
