//
//  ProfileSheetView.swift
//  Tog
//
//  Created by Marco Margarucci on 20/10/21.
//

import SwiftUI

struct ProfileSheetView: View {
    // MARK: - Properties
    
    // User profile
    var profile: TogProfile
    
    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                // Avatar image
                Image(uiImage: profile.avatarImage)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 110, height: 110)
                    .clipShape(Circle())
                    .shadow(color: .black.opacity(0.5), radius: 4, x: 0, y: 6)
                    .accessibilityHidden(true)
                // First name and last name
                Text(profile.name + " " + profile.lastName)
                    .bold()
                    .font(.title2)
                    .minimumScaleFactor(0.9)
                // Company name
                Text(profile.companyName)
                    .fontWeight(.semibold)
                    .minimumScaleFactor(0.9)
                    .foregroundColor(.secondary)
                    .accessibilityLabel(Text("Works at \(profile.companyName)"))
                // Biography
                Text(profile.bio)
                    .accessibilityLabel(Text("Bio, \(profile.bio)"))
            }
            .padding()
        }
    }
}

struct ProfileSheetView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSheetView(profile: TogProfile(record: MockData.profile))
    }
}
