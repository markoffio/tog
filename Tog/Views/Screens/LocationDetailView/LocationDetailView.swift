//
//  LocationDetailView.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI

struct LocationDetailView: View {
    // MARK: - Properties
        
    // View model
    @ObservedObject var viewModel: LocationDetailViewModel
    // Size category
    @Environment(\.sizeCategory) var sizeCategory
    
    var body: some View {
        ZStack {
            VStack(spacing: 10) {
                BannerImageView(image: viewModel.location.bannerImage)
                    //.accessibilityHidden(true)
                AddressView(address: viewModel.location.address)
                DescriptionView(description: viewModel.location.description)
                ActionButtons(viewModel: viewModel)
                GridHeaderTextView(number: viewModel.checkedInProfiles.count, isEmpty: viewModel.checkedInProfiles.isEmpty)
                AvatarGridView(viewModel: viewModel)
            }
            .accessibilityHidden(viewModel.isShowingProfileModal)
            .blur(radius: viewModel.isShowingProfileModal ? 50 : 0)
            //.transition(.opacity)
            //.animation(.easeInOut)
            //.zIndex(1)
            // Loading view
            if viewModel.isShowingProfileModal {
                ProfileModalView(profile: viewModel.selectedProfile!, isShowingProfileModal: $viewModel.isShowingProfileModal)
            }
        }
        .onAppear {
            viewModel.getCheckedInProfiles()
            viewModel.getCheckedInStatus()
        }
        .sheet(isPresented: $viewModel.isShowingProfileSheet, content: {
            NavigationView {
                ProfileSheetView(profile: viewModel.selectedProfile!)
                    .toolbar {
                        Button {
                            viewModel.isShowingProfileSheet = false
                        } label: {
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.mapMarker)
                                .frame(width: 25)
                        }

                    }
            }
        })
        .alert(item: $viewModel.alertItem, content: { $0.alert })
        .navigationTitle(viewModel.location.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct LocationDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LocationDetailView(viewModel: LocationDetailView.LocationDetailViewModel(location: TogLocation(record: MockData.storieDiPane)))
        }
    }
}

fileprivate struct FirstNameAvatarView: View {
    // Size
    var size: CGFloat
    // User profile
    var profile: TogProfile
    
    var body: some View {
        VStack {
            AvatarView(size: size, image: profile.avatarImage)
            Text(profile.name)
                .bold()
                .lineLimit(1)
                .minimumScaleFactor(0.75)
                .foregroundColor(.gray)
        }
        .accessibilityElement(children: .ignore)
        .accessibilityAddTraits(.isButton)
        .accessibilityHint(Text("Show's \(profile.name) profile pop up"))
        .accessibilityLabel(Text("\(profile.name) \(profile.lastName)"))
    }
}

fileprivate struct BannerImageView: View {
    // Image
    var image: UIImage
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .scaledToFill()
            .frame(height: 120)
            .clipped()
    }
}

fileprivate struct AddressLabel: View {
    // Address
    var address: String
    
    var body: some View {
        Label(address, systemImage: "mappin.and.ellipse")
            .font(.caption)
            .foregroundColor(.brandPrimary)
    }
}

fileprivate struct DescriptionView: View {
    // Description
    var description: String
    
    var body: some View {
        Text(description)
            .multilineTextAlignment(.center)
            .fixedSize(horizontal: false, vertical: true)
            .minimumScaleFactor(0.75)
            .padding(.horizontal)
    }
}

// Address view
fileprivate struct AddressView: View {
    // Address
    var address: String
    
    var body: some View {
        HStack {
            Label(address, systemImage: "mappin.and.ellipse")
                .font(.caption)
                .foregroundColor(.brandPrimary)
            Spacer()
        }
        .padding(.horizontal)
    }
}


// Grid heade text view
fileprivate struct GridHeaderTextView: View {
    // Number of users checked in
    var number: Int
    // Used to check if there are users in a specific location
    var isEmpty: Bool
    
    var body: some View {
        Text(isEmpty ? "" : "I tuoi amici")
            .font(.title3)
            .foregroundColor(.gray)
            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            .accessibilityAddTraits(.isHeader)
            .accessibilityLabel(Text("Ci sono \(number) tuoi amici"))
            .accessibilityHint(Text("Bottom section is scrollable"))
    }
}

fileprivate struct GridEmptyStateView: View {
    var body: some View {
        VStack(spacing: 10) {
            Image("no_users")
                .resizable()
                .scaledToFill()
                .opacity(0.5)
                .frame(width: 80, height: 80)
            Text("Non c'è nessuno qui...")
                .font(.title3)
                .foregroundColor(.gray)
                .fontWeight(.semibold)
        }
        .padding(.top, 40)
    }
}

// Action buttons
fileprivate struct ActionButtons: View {
    
    // View model
    @ObservedObject var viewModel: LocationDetailView.LocationDetailViewModel
    
    var body: some View {
        HStack(spacing: 20) {
            Button(action: { viewModel.getDirectionsToLocation() }, label: {
                LocationActionButton(imageColor: .white, circleColor: .brandSecondary, imageName: "location.fill")
                
            })
                .accessibilityLabel(Text("Get directions."))
            Link(destination: URL(string: viewModel.location.websiteURL)!, label: {
                LocationActionButton(imageColor: .white, circleColor: .brandSecondary, imageName: "network")
                
            })
                .accessibilityRemoveTraits(.isButton)
                .accessibilityLabel(Text("Go to website"))
            Button(action: { viewModel.callLocation() }, label: {
                LocationActionButton(imageColor: .white, circleColor: .brandSecondary, imageName: "phone.fill")
                
            })
                .accessibilityLabel(Text("Call location"))
            // Show check in / check out button only if the user has a profile
            if let _ = CloudKitManager.shared.profileRecordID {
                Button(action: {
                    viewModel.updateCheckInStatus(to: viewModel.isCheckedIn ? .checkedOut : .checkedIn)
                }, label: {
                    LocationActionButton(imageColor: .white, circleColor: viewModel.buttonColor, imageName: viewModel.buttonImage)
                        .accentColor(.brandPrimary)
                    
                })
                    .accessibilityLabel(Text(viewModel.buttonAccessibilityLabel))
                    .disabled(viewModel.isLoading)
            }
        }
    }
}

// Avatar grid view
fileprivate struct AvatarGridView: View {
    // View model
    @ObservedObject var viewModel: LocationDetailView.LocationDetailViewModel
    // Size category
    @Environment(\.sizeCategory) var sizeCategory
    
    var body: some View {
        ZStack {
            // Check if there are no users checked in
            if viewModel.checkedInProfiles.isEmpty {
                GridEmptyStateView()
            } else {
                ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false) {
                    // Avatars grid
                    LazyVGrid(columns: viewModel.determineColumns(for: sizeCategory), content: {
                        ForEach(viewModel.checkedInProfiles) { profile in
                            FirstNameAvatarView(size: sizeCategory >= .accessibilityMedium ? 100 : 64, profile: profile)
                                .onTapGesture {
                                    viewModel.show(profile: profile, in: sizeCategory)
                                }
                        }
                    })
                }
                .padding(.horizontal)
            }
            if viewModel.isLoading { LoadingView() }
        }
        Spacer()
    }
}
