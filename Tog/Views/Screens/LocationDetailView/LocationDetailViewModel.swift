//
//  LocationDetailViewModel.swift
//  Tog
//
//  Created by Marco Margarucci on 30/09/21.
//

import Foundation
import SwiftUI
import MapKit
import CloudKit

enum CheckInStatus { case checkedIn, checkedOut }

extension LocationDetailView {
    final class LocationDetailViewModel: ObservableObject {
        // MARK: - Properties
        
        // Location
        var location: TogLocation
        // Selected profile
        var selectedProfile: TogProfile?
        // Alert item
        @Published var alertItem: AlertItem?
        // Show modal view
        @Published var isShowingProfileModal: Bool = false
        // Show sheet
        @Published var isShowingProfileSheet: Bool = false
        // Checked in profiles
        @Published var checkedInProfiles: [TogProfile] = []
        // Used to check if a specific user is checked in or not
        @Published var isCheckedIn: Bool = false
        // Is loading
        @Published var isLoading: Bool = false
        // Button color
        var buttonColor: Color {
            isCheckedIn ? .controlsSecondary : .controlsPrimary
        }
        // Button image
        var buttonImage: String {
            isCheckedIn ? "arrowshape.turn.up.forward.circle" : "checkmark.circle.fill"
        }
        // Accessibility label
        var buttonAccessibilityLabel: String {
            isCheckedIn ? "Check out of location" : "Check into location"
        }
        
        // MARK: - Functions
        
        init(location: TogLocation) {
            self.location = location
        }
        
        // Show profile
        func show(profile: TogProfile, in sizeCategory: ContentSizeCategory) {
            selectedProfile = profile
            if sizeCategory >= .accessibilityMedium {
                // Show the sheet
                isShowingProfileSheet = true
            } else {
                isShowingProfileModal = true
            }
        }
        
        // Determine the number of columns for the grid based on the current size category
        func determineColumns(for sizeCategory: ContentSizeCategory) -> [GridItem] {
            let numberOfColumns = sizeCategory >= .accessibilityMedium ? 1 : 3
            return Array(repeating: GridItem(.flexible()), count: numberOfColumns)
        }
        
        // Get directions to location
        // Open information in Maps
        func getDirectionsToLocation() {
            let placemark = MKPlacemark(coordinate: location.location.coordinate)
            let mapItem = MKMapItem(placemark: placemark)
            // Set location name
            mapItem.name = location.name
            // Set location phone number
            mapItem.phoneNumber = location.phoneNumber
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking])
        }
        
        // Call location
        func callLocation() {
            guard let url = URL(string: "tel://\(location.phoneNumber)") else {
                alertItem = AlertContext.invalidPhoneNumber
                return
            }
            UIApplication.shared.open(url)
        }
        
        func getCheckedInStatus() {
            // Fetch user profile from cloud database
            guard let profileRecordID = CloudKitManager.shared.profileRecordID else { return }
            CloudKitManager.shared.fetchRecord(with: profileRecordID) { [self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let record):
                        if let reference = record[TogProfile.kIsCheckedIn] as? CKRecord.Reference {
                            isCheckedIn = reference.recordID == location.id
                        }
                        else { isCheckedIn = false }
                    case .failure(_):
                        alertItem = AlertContext.unableToGetCheckInStatus
                    }
                }
            }
        }
        
        // Update check in status
        func updateCheckInStatus(to checkInStatus: CheckInStatus) {
            // Fetch user profile from cloud database
            guard let profileRecordID = CloudKitManager.shared.profileRecordID else {
                alertItem = AlertContext.unableToGetProfile
                return
            }
            showLoadingView()
            CloudKitManager.shared.fetchRecord(with: profileRecordID) { [self] result in
                switch result {
                case .success(let record):
                    // Create reference to the location
                    switch checkInStatus {
                    case .checkedIn:
                        record[TogProfile.kIsCheckedIn] = CKRecord.Reference(recordID: location.id, action: .none)
                        record[TogProfile.kIsCheckedInNilCheck] = 1
                    case .checkedOut:
                        record[TogProfile.kIsCheckedIn] = nil
                        record[TogProfile.kIsCheckedInNilCheck] = nil
                    }
                    // Save the updated profile to cloud database
                    CloudKitManager.shared.save(record: record) { result in
                        DispatchQueue.main.async {
                            hideLoadingView()
                            switch result {
                            case .success(let record):
                                HapticManager.playSuccess()
                                // Get user profile
                                let profile = TogProfile(record: record)
                                switch checkInStatus {
                                case .checkedIn:
                                    // Add the user to the array of checked in profiles
                                    checkedInProfiles.append(profile)
                                case .checkedOut:
                                    // Remove the user to the array of checked in profiles
                                    checkedInProfiles.removeAll(where: { $0.id == profile.id })
                                }
                                isCheckedIn.toggle()
                            case .failure(_):
                                alertItem = AlertContext.unableToCheckInOrOut
                            }
                        }
                    }
                case .failure(_):
                    hideLoadingView()
                    alertItem = AlertContext.unableToCheckInOrOut
                }
            }
        }
        
        // Get checked in profiles
        func getCheckedInProfiles() {
            showLoadingView()
            CloudKitManager.shared.getCheckedInProfiles(for: location.id) { [self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let profiles):
                        checkedInProfiles = profiles
                    case .failure(_):
                        alertItem = AlertContext.unableToGetCheckedInProfiles
                    }
                    hideLoadingView()
                }
            }
        }
        
        // Show loading view
        private func showLoadingView() { isLoading = true }
        
        // Hide loading view
        private func hideLoadingView() { isLoading = false }
    }
}
