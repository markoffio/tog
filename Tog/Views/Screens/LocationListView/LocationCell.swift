//
//  LocationCell.swift
//  Tog
//
//  Created by Marco Margarucci on 24/08/21.
//

import SwiftUI

struct LocationCell: View {
    // MARK: - Properties
    // Location
    var location: TogLocation
    // Profiles
    var profiles: [TogProfile]
    
    var body: some View {
        HStack {
            Image(uiImage: location.squareImage)
                .resizable()
                .scaledToFit()
                .frame(width: 75, height: 75)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .padding(.vertical, 8)
            VStack(alignment: .leading) {
                Text(location.name)
                    .font(.body)
                    .fontWeight(.semibold)
                    .lineLimit(1)
                    .minimumScaleFactor(0.75)
                    .fixedSize()
                if profiles.isEmpty {
                    Text("Non c'è nessuno qui...")
                        .fontWeight(.semibold)
                        .foregroundColor(.secondary)
                        .padding(.top, 2)
                } else {
                    HStack {
                        ForEach(profiles.indices, id: \.self) { index in
                            if index <= 3 || (index == 4 && profiles.count == 5) {
                                AvatarView(size: 35, image: profiles[index].avatarImage)
                            } else if index == 4 && profiles.count > 5 {
                                AdditionalProfilesView(number: min(profiles.count - 4, 99))
                            }
                        }
                    }
                }
            }
            .padding(.leading)
        }
    }
}

fileprivate struct AdditionalProfilesView: View {
    // This number is used to show how many other users are checked in a specific location
    var number: Int
    
    var body: some View {
        Text("+\(number)")
            .font(.system(size: 14, weight: .semibold))
            .frame(width: 35, height: 35)
            .foregroundColor(.white)
            .background(Color.darkBlue)
            .clipShape(Circle())
    }
}

/*
struct LocationCell_Previews: PreviewProvider {
    static var previews: some View {
        LocationCell(location: TogLocation(record: MockData.location, profiles: []))
    }
}
*/
