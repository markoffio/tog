//
//  LocationListViewModel.swift
//  Tog
//
//  Created by Marco Margarucci on 04/10/21.
//

import CloudKit
import SwiftUI

extension LocationListView {
    final class LocationListViewModel: ObservableObject {
        // MARK: - Properties
        
        // Checked in profiles
        @Published var checkedInProfiles: [CKRecord.ID: [TogProfile]] = [:]
        // Alert item
        @Published var alertItem: AlertItem?
        
        // MARK: - Functions
        
        // Get checked in profiles dictionary
        func getCheckedInProfilesDictionary() {
            CloudKitManager.shared.getCheckedInProfilesDictionary { result in
                DispatchQueue.main.async { [self] in
                    switch result {
                    case .success(let checkedInProfiles):
                        self.checkedInProfiles = checkedInProfiles
                    case .failure(_):
                        alertItem = AlertContext.unableToGetAllCheckedInProfiles
                    }
                }
            }
        }
        
        // Create voice over summary
        func createVoiceOverSummary(for location: TogLocation) -> String {
            let count = checkedInProfiles[location.id, default: []].count
            let personPlurality = count == 1 ? "person" : "people"
            return "\(location.name) \(count) \(personPlurality) checkedin"
        }
        
        // Create location detail view based on different content size category
        @ViewBuilder func createLocationDetailView(for location: TogLocation, in sizeCategory: ContentSizeCategory) -> some View {
            if sizeCategory >= .accessibilityMedium {
                LocationDetailView(viewModel: LocationDetailView.LocationDetailViewModel(location: location)).embedInScrollView(axis: .vertical, showsIndicators: false)
            } else {
                LocationDetailView(viewModel: LocationDetailView.LocationDetailViewModel(location: location))
            }
        }
    }

}
