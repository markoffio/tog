//
//  LocationListView.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI

struct LocationListView: View {
    // MARK: - Properties
    
    // Location manager
    @EnvironmentObject private var locationManager: LocationManager
    // View model
    @StateObject private var viewModel = LocationListViewModel()
    // Size category
    @Environment(\.sizeCategory) var sizeCategory
    // On appear has fired
    @State private var onAppearHasFired = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(locationManager.locations) { location in
                    NavigationLink(destination: viewModel.createLocationDetailView(for: location, in: sizeCategory)) {
                        LocationCell(location: location, profiles: viewModel.checkedInProfiles[location.id, default: []])
                            .accessibilityElement(children: .ignore)
                            .accessibilityLabel(Text(viewModel.createVoiceOverSummary(for: location)))
                    }
                }
            }
            .navigationTitle("Località")
            .onAppear {
                if !onAppearHasFired {
                    onAppearHasFired = true
                    viewModel.getCheckedInProfilesDictionary()
                }
            }
            .alert(item: $viewModel.alertItem, content: { $0.alert })
        }
    }
}

struct LocationListView_Previews: PreviewProvider {
    static var previews: some View {
        LocationListView()
    }
}
