//
//  ProfileView.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI
import CloudKit

struct ProfileView: View {

    // MARK: - Properties
    
    // View model
    @StateObject private var viewModel = ProfileViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                HStack(spacing: 16) {
                    ProfileImageView(viewModel: viewModel)
                    VStack(spacing: 1) {
                        TextField("Nome", text: $viewModel.firstName)
                            .profileNameStyle()
                        TextField("Cognome", text: $viewModel.lastName)
                            .profileNameStyle()
                        TextField("Società", text: $viewModel.companyName)
                    }
                    .padding(.trailing, 16)
                }
                .padding(.vertical)
                .background(Color.clear)
                .cornerRadius(12)
                .padding(.horizontal)
                VStack(alignment: .leading, spacing: 8) {
                    HStack {
                        CharactersRemainView(currentCount: viewModel.bio.count)
                            .accessibilityAddTraits(.isHeader)
                        Spacer()
                        if viewModel.isCheckedIn {
                            Button(action: {
                                viewModel.checkOut()
                            } , label: {
                                Label("Check out", systemImage: "arrowshape.turn.up.right.fill")
                                    .foregroundColor(.white)
                                    .padding(10)
                                    .frame(height: 30)
                                    .background(Color.controlsSecondary)
                                    .cornerRadius(8)
                                    .accessibilityLabel(Text("Check out"))
                            })
                            .disabled(viewModel.isLoading)
                        }
                    }
                    BioTextEditor(text: $viewModel.bio)
                }
                .padding(.horizontal, 20)
                Spacer()
                // Create/Update profile button
                Button(action: { viewModel.buttonAction() }, label: {
                    RoundedButton(text: viewModel.buttonTitle)
                })
                .padding(.bottom, 25)
                .padding(.horizontal, 20)
            }
            .blur(radius: viewModel.isLoading ? 5 : 0)
            // Loading view
            if viewModel.isLoading { LoadingView() }
        }
        .navigationTitle("Il tuo profilo")
        .navigationBarTitleDisplayMode(DeviceTypes.isiPhone8Standard ? .inline : .automatic)
        .toolbar(content: {
            Button {
                dismissKeyboard()
            } label: {
                Image(systemName: "keyboard.chevron.compact.down")
            }
        })
        .onAppear {
            viewModel.getProfile()
            viewModel.getCheckedInStatus()
        }
        // IMPORTANT: THIS ALERT IS DEPRECATED IN iOS 15
        .alert(item: $viewModel.alertItem, content: { $0.alert })
        .sheet(isPresented: $viewModel.isShowingPhotoPicker) {
            PhotoPicker(image: $viewModel.avatar)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ProfileView()
        }
    }
}

// Name background view
fileprivate struct NameBackgroundView: View {
    var body: some View {
        Color(.secondarySystemBackground)
            .frame(height: 130)
            .cornerRadius(16)
            .padding(.horizontal)
    }
}

// Profile image view
fileprivate struct ProfileImageView: View {
    // View model
    @ObservedObject var viewModel: ProfileView.ProfileViewModel
    
    var body: some View {
        ZStack {
            AvatarView(size: 84, image: viewModel.avatar)
            Image(systemName: "plus.circle.fill")
                .renderingMode(.original)
                .foregroundColor(.gray)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(.secondarySystemBackground), lineWidth: 2))
                .offset(x: 28, y: 36)
        }
        .accessibilityElement(children: .ignore)
        .accessibilityAddTraits(.isButton)
        .accessibilityLabel(Text("Profile photo"))
        .accessibilityHint(Text("Opens the iPhone's photo picker"))
        .padding(.leading, 12)
        .onTapGesture { viewModel.isShowingPhotoPicker = true }
    }
}

// Characters remain view
fileprivate struct CharactersRemainView: View {
    // Characters count
    var currentCount: Int
    
    var body: some View {
        Text("Bio: ")
            .font(.callout)
            .foregroundColor(.brandSecondary)
        +
            Text("\(100 - currentCount) ")
            .bold()
            .font(.callout)
            .foregroundColor(currentCount < 100 ? .darkBlue : .brandPrimary)
        +
        Text("caratteri rimanenti")
            .font(.callout)
            .foregroundColor(.brandSecondary)
    }
}

struct BioTextEditor: View {
    // Text
    var text: Binding<String>
    
    var body: some View {
        TextEditor(text: text)
            .frame(height: 100)
            .overlay(RoundedRectangle(cornerRadius: 8).stroke(Color.brandSecondary, lineWidth: 1))
            .font(.system(size: 14, weight: .semibold))
            .foregroundColor(.secondary)
            .accessibilityHint(Text("Non puoi inserire più di cento caratteri"))
    }
}
