//
//  ProfileViewModel.swift
//  Tog
//
//  Created by Marco Margarucci on 28/09/21.
//

import CloudKit

enum ProfileContext { case create, update }

extension ProfileView {
    final class ProfileViewModel: ObservableObject {
        // MARK: - Properties
        
        // First name
        @Published var firstName: String            = ""
        // Last name
        @Published var lastName: String             = ""
        // Company name
        @Published var companyName: String          = ""
        // Bio
        @Published var bio: String                  = ""
        // Avatar
        @Published var avatar                       = PlaceholderImage.avatar
        // Used to show the photo picker
        @Published var isShowingPhotoPicker: Bool   = false
        // Is loading
        @Published var isLoading: Bool              = false
        // Alert item
        @Published var alertItem: AlertItem?
        // Used to check if a specific user is checked in or not
        @Published var isCheckedIn: Bool = false
        // Profile context
        var profileContext: ProfileContext = .create
        // Profile record
        private var existingProfileRecord: CKRecord? {
            didSet {
                profileContext = .update
            }
        }
        // Button title
        var buttonTitle: String {
            profileContext == .create ? "CREA PROFILO" : "AGGIORNA PROFILO"
        }
        // MARK: - Functions
        
        // Button action
        func buttonAction() { profileContext == .create ? createUserProfile() : updateUserProfile() }
        
        // Get profile
        func getProfile() {
            guard let userRecord = CloudKitManager.shared.userRecord else {
                alertItem = AlertContext.noUserRecord
                return
            }
            
            // Profile reference
            guard let profileReference = userRecord["userProfile"] as? CKRecord.Reference else { return }
            let profileRecordID = profileReference.recordID
            
            showLoadingView()
            
            CloudKitManager.shared.fetchRecord(with: profileRecordID) { result in
                DispatchQueue.main.async { [self] in
                    hideLoadingView()
                    
                    switch result {
                    case .success(let record):
                        existingProfileRecord = record
                        let profile = TogProfile(record: record)
                        firstName = profile.name
                        lastName = profile.lastName
                        companyName = profile.companyName
                        bio = profile.bio
                        avatar = profile.avatarImage
                    case .failure(_):
                        alertItem = AlertContext.unableProfileFailure
                        break
                    }
                }
            }
        }
        
        // Create user profile
        private func createUserProfile() {
            guard isProfileValid() else {
                alertItem = AlertContext.invalidProfile
                return
            }
            
            let profileRecord = createProfileRecord()
            
            guard let userRecord = CloudKitManager.shared.userRecord else {
                alertItem = AlertContext.noUserRecord
                return
            }
            
            // Create reference on UserRecord to the TogProfile
            userRecord["userProfile"] = CKRecord.Reference(recordID: profileRecord.recordID, action: .none)
            
            showLoadingView()
            
            CloudKitManager.shared.batchSave(records: [userRecord, profileRecord]) { result in
                DispatchQueue.main.async { [self] in
                    hideLoadingView()
                    
                    switch result {
                    case .success(let records):
                        for record in records where record.recordType == RecordType.profile {
                            existingProfileRecord = record
                            CloudKitManager.shared.profileRecordID = record.recordID
                        }
                        alertItem = AlertContext.createProfileSuccess
                    case .failure(_):
                        alertItem = AlertContext.createProfileFailure
                        break
                    }
                }
            }
        }
        
        // Update user profile
        private func updateUserProfile() {
            guard isProfileValid() else {
                alertItem = AlertContext.invalidProfile
                return
            }
            
            guard let profileRecord = existingProfileRecord else {
                alertItem = AlertContext.unableProfileFailure
                return
            }
            // Populate fields
            profileRecord[TogProfile.kFirstName] = firstName
            profileRecord[TogProfile.kLastName] = lastName
            profileRecord[TogProfile.kCompanyName] = companyName
            profileRecord[TogProfile.kBio] = bio
            profileRecord[TogProfile.kAvatar] = avatar.convertToCKAsset()
            
            showLoadingView()
            
            CloudKitManager.shared.save(record: profileRecord) { result in
                DispatchQueue.main.async { [self] in
                    hideLoadingView()
                    switch result {
                    case .success(_):
                        alertItem = AlertContext.updateProfileSuccess
                    case .failure(_):
                        alertItem = AlertContext.updateProfileFailure
                    }
                }
            }
        }
        
        // Check if the profile is valid
        func isProfileValid() -> Bool {
            guard !firstName.isEmpty,
                  !lastName.isEmpty,
                  !companyName.isEmpty,
                  !bio.isEmpty,
                  avatar != PlaceholderImage.avatar,
                  bio.count <= 100 else { return false }
            return true
        }
        
        // Create profile record
        func createProfileRecord() -> CKRecord {
            // Create CKRecord
            let profileRecord = CKRecord(recordType: RecordType.profile)
            profileRecord[TogProfile.kFirstName] = firstName
            profileRecord[TogProfile.kLastName] = lastName
            profileRecord[TogProfile.kCompanyName] = companyName
            profileRecord[TogProfile.kBio] = bio
            profileRecord[TogProfile.kAvatar] = avatar.convertToCKAsset()
            return profileRecord
        }
        
        // Check if the user is currently checked in any location
        func getCheckedInStatus() {
            // Fetch user profile from cloud database
            guard let profileRecordID = CloudKitManager.shared.profileRecordID else { return }
            CloudKitManager.shared.fetchRecord(with: profileRecordID) { [self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let record):
                        if let _ = record[TogProfile.kIsCheckedIn] as? CKRecord.Reference { isCheckedIn = true }
                        else { isCheckedIn = false }
                    case .failure(_):
                        break
                    }
                }
            }
        }
        
        // Perform the checkout
        func checkOut() {
            guard let profileID = CloudKitManager.shared.profileRecordID else {
                alertItem = AlertContext.unableToGetProfile
                return
            }
            showLoadingView()
            CloudKitManager.shared.fetchRecord(with: profileID) { [self] result in
                switch result {
                case .success(let record):
                    record[TogProfile.kIsCheckedIn] = nil
                    record[TogProfile.kIsCheckedInNilCheck] = nil
                    CloudKitManager.shared.save(record: record) { [self] result in
                        
                        DispatchQueue.main.async {
                            hideLoadingView()
                            switch result {
                            case .success(_):
                                HapticManager.playSuccess()
                                isCheckedIn = false
                            case .failure(_):
                                alertItem = AlertContext.unableToCheckInOrOut
                            }
                        }
                    }
                case .failure(_):
                    hideLoadingView()
                    DispatchQueue.main.async { self.alertItem = AlertContext.unableToCheckInOrOut }
                }
            }
        }
        
        // Show loading view
        private func showLoadingView() {
            isLoading = true
        }
        
        // Hide loading view
        private func hideLoadingView() {
            isLoading = false
        }
    }
}
