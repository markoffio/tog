//
//  AppTabView.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import SwiftUI

struct AppTabView: View {
    // MARK: - Properties
    // View model
    @State private var viewModel = AppTabViewModel()
    
    var body: some View {
        TabView {
            LocationMapView()
                .tabItem { Label("Mappa", systemImage: "map") }
            LocationListView()
                .tabItem { Label("Località", systemImage: "list.bullet") }
            NavigationView { ProfileView() }
            .tabItem { Label("Profilo", systemImage: "person") }
        }
        .onAppear {
            CloudKitManager.shared.getUserRecord()
            viewModel.runStartupChecks()
        }
        .accentColor(.brandPrimary)
        .sheet(isPresented: $viewModel.isShowingOnboardingView, onDismiss: viewModel.checkIfLocationServiceIsEnabled) {
            OnboardingView()
        }
    }
}

struct AppTabView_Previews: PreviewProvider {
    static var previews: some View {
        AppTabView()
    }
}
