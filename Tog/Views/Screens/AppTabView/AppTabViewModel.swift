//
//  AppTabViewModel.swift
//  Tog
//
//  Created by Marco Margarucci on 06/10/21.
//

import Foundation
import CoreLocation
import SwiftUI

extension AppTabView {
    final class AppTabViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
        // MARK: - Properties
        
        // Used to show or not the onboarding view
        @Published var isShowingOnboardingView: Bool = false
        // Alert item
        @Published var alertItem: AlertItem?
        // App storage
        @AppStorage(OnboardingViewShow.kHasSeenOnboardingView) var hasSeenOnboardingView = false {
            didSet { isShowingOnboardingView = hasSeenOnboardingView }
        }
        
        // Device location manager
        var deviceLocationManager: CLLocationManager?
        
        // MARK: - Functions
        
        // Check if the onboarding view has never been shown.
        // If the onboarding view has never been shown, shows it: otherwise check if location services are enabled
        func runStartupChecks() {
            if !hasSeenOnboardingView {
                hasSeenOnboardingView = true
            } else {
                checkIfLocationServiceIsEnabled()
            }
        }
        
        // Check if the location service is enabled
        func checkIfLocationServiceIsEnabled() {
            if CLLocationManager.locationServicesEnabled() {
                deviceLocationManager = CLLocationManager()
                deviceLocationManager!.delegate = self
            } else { // Show an alert
                alertItem = AlertContext.locationDisabled
            }
        }
        
        // Check location authorization
        private func checkLocationAuthorization() {
            guard let deviceLocationManager = deviceLocationManager else { return }
            switch deviceLocationManager.authorizationStatus {
            case .notDetermined:
                deviceLocationManager.requestWhenInUseAuthorization()
            case .restricted:
                alertItem = AlertContext.locationRestricted
            case .denied:
                alertItem = AlertContext.locationDenied
            case .authorizedAlways, .authorizedWhenInUse:
                break
            @unknown default:
                break
            }
        }
        
        func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
            checkLocationAuthorization()
        }
    }
}
