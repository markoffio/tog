//
//  CustomAnnotation.swift
//  Tog
//
//  Created by Marco Margarucci on 04/10/21.
//

import SwiftUI

struct CustomAnnotation: View {
    // MARK: - Properties
    // Location
    var location: TogLocation
    // Number of profiles checked in a location
    var number: Int
    
    var body: some View {
        VStack {
            ZStack {
                MapAnnotationShape()
                    .frame(width: 96, height: 45)
                    .foregroundColor(.mapAnnotation)
                HStack {
                    Image(uiImage: location.squareImage)
                        .resizable()
                        .frame(width: 30, height: 30)
                        .clipShape(Circle())
                        .offset(y: -4)
                    if number > 0 {
                        Spacer()
                            .frame(width: 28)
                        Text("\(min(number, 99))")
                            .font(.system(size: 11, weight: .bold))
                            .frame(width: 26, height: 18)
                            .foregroundColor(.white)
                            .background(Color.mapMarker)
                            .clipShape(Capsule())
                            .offset(y: -3)
                    }
                }
                .padding(.horizontal)
            }
            Text(location.name)
                .font(.caption)
                .fontWeight(.semibold)
        }
    }
}

struct CustomAnnotation_Previews: PreviewProvider {
    static var previews: some View {
        CustomAnnotation(location: TogLocation(record: MockData.location), number: 20)
    }
}
