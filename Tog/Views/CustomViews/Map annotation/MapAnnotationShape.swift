//
//  MapBalloon.swift
//  Tog
//
//  Created by Marco Margarucci on 04/10/21.
//

import SwiftUI

struct MapAnnotationShape: Shape {
    func path(in rect: CGRect) -> Path {
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 95.99, y: 0.2))
        bezierPath.addLine(to: CGPoint(x: 96.11, y: 0.22))
        bezierPath.addCurve(to: CGPoint(x: 97.78, y: 1.89), controlPoint1: CGPoint(x: 96.88, y: 0.51), controlPoint2: CGPoint(x: 97.49, y: 1.12))
        bezierPath.addCurve(to: CGPoint(x: 98, y: 4.59), controlPoint1: CGPoint(x: 98, y: 2.61), controlPoint2: CGPoint(x: 98, y: 3.27))
        bezierPath.addLine(to: CGPoint(x: 98, y: 34.41))
        bezierPath.addCurve(to: CGPoint(x: 97.8, y: 36.99), controlPoint1: CGPoint(x: 98, y: 35.73), controlPoint2: CGPoint(x: 98, y: 36.39))
        bezierPath.addLine(to: CGPoint(x: 97.78, y: 37.11))
        bezierPath.addCurve(to: CGPoint(x: 96.11, y: 38.78), controlPoint1: CGPoint(x: 97.49, y: 37.88), controlPoint2: CGPoint(x: 96.88, y: 38.49))
        bezierPath.addCurve(to: CGPoint(x: 93.41, y: 39), controlPoint1: CGPoint(x: 95.39, y: 39), controlPoint2: CGPoint(x: 94.73, y: 39))
        bezierPath.addLine(to: CGPoint(x: 57.72, y: 39))
        bezierPath.addCurve(to: CGPoint(x: 49, y: 49), controlPoint1: CGPoint(x: 56.11, y: 40.84), controlPoint2: CGPoint(x: 49, y: 49))
        bezierPath.addCurve(to: CGPoint(x: 40.28, y: 39), controlPoint1: CGPoint(x: 49, y: 49), controlPoint2: CGPoint(x: 41.89, y: 40.84))
        bezierPath.addLine(to: CGPoint(x: 4.59, y: 39))
        bezierPath.addCurve(to: CGPoint(x: 2.01, y: 38.8), controlPoint1: CGPoint(x: 3.27, y: 39), controlPoint2: CGPoint(x: 2.61, y: 39))
        bezierPath.addLine(to: CGPoint(x: 1.89, y: 38.78))
        bezierPath.addCurve(to: CGPoint(x: 0.22, y: 37.11), controlPoint1: CGPoint(x: 1.12, y: 38.49), controlPoint2: CGPoint(x: 0.51, y: 37.88))
        bezierPath.addCurve(to: CGPoint(x: 0, y: 34.41), controlPoint1: CGPoint(x: 0, y: 36.39), controlPoint2: CGPoint(x: 0, y: 35.73))
        bezierPath.addLine(to: CGPoint(x: 0, y: 4.59))
        bezierPath.addCurve(to: CGPoint(x: 0.2, y: 2.01), controlPoint1: CGPoint(x: 0, y: 3.27), controlPoint2: CGPoint(x: 0, y: 2.61))
        bezierPath.addLine(to: CGPoint(x: 0.22, y: 1.89))
        bezierPath.addCurve(to: CGPoint(x: 1.89, y: 0.22), controlPoint1: CGPoint(x: 0.51, y: 1.12), controlPoint2: CGPoint(x: 1.12, y: 0.51))
        bezierPath.addCurve(to: CGPoint(x: 4.59, y: 0), controlPoint1: CGPoint(x: 2.61, y: 0), controlPoint2: CGPoint(x: 3.27, y: 0))
        bezierPath.addLine(to: CGPoint(x: 93.41, y: 0))
        bezierPath.addCurve(to: CGPoint(x: 95.99, y: 0.2), controlPoint1: CGPoint(x: 94.73, y: 0), controlPoint2: CGPoint(x: 95.39, y: -0))
        bezierPath.close()
        UIColor.gray.setFill()
        bezierPath.fill()

        let path = Path(bezierPath.cgPath)

        return path
    }
}

struct MapBalloon_Previews: PreviewProvider {
    static var previews: some View {
        MapAnnotationShape().frame(width: 70, height: 70)
    }
}
