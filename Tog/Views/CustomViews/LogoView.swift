//
//  LogoView.swift
//  Tog
//
//  Created by Marco Margarucci on 25/09/21.
//

import SwiftUI

struct LogoView: View {
    // MARK: - Properties
    
    // Frame width
    var frameWidth: CGFloat
    
    var body: some View {
        Image("icon")
            .resizable()
            .scaledToFit()
            .frame(width: frameWidth)
    }
}

struct LogoView_Previews: PreviewProvider {
    static var previews: some View {
        LogoView(frameWidth: 100)
    }
}
