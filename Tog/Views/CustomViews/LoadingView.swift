//
//  LoadingView.swift
//  Tog
//
//  Created by Marco Margarucci on 29/09/21.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .opacity(0.9)
                .ignoresSafeArea()
            RoundedRectangle(cornerRadius: 26.0)
                .opacity(0.1)
                .frame(width: 110, height: 110)
                .offset(y: -40)
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .brandPrimary))
                .scaleEffect(2)
                .offset(y: -40)
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
