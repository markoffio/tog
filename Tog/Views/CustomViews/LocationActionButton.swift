//
//  LocationActionButton.swift
//  Tog
//
//  Created by Marco Margarucci on 24/08/21.
//

import SwiftUI

struct LocationActionButton: View {
    // Circle color
    var imageColor: Color
    // Circle color
    var circleColor: Color
    // Image name
    var imageName: String
    
    var body: some View {
        ZStack {
            Circle()
                .foregroundColor(circleColor)
                .frame(width: 60, height: 60)
            Image(systemName: imageName)
                .resizable()
                .scaledToFit()
                .foregroundColor(imageColor)
                .frame(width: 22, height: 22, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
    }
}

/*
struct LocationActionButton_Previews: PreviewProvider {
    static var previews: some View {
        LocationActionButton()
    }
}
*/
