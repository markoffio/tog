//
//  RoundedButton.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI

struct RoundedButton: View {
    // Button text
    var text: String = ""

    var body: some View {
        Text(text)
            .font(.system(size: 16, weight: .bold))
            .frame(height: 55)
            .frame(maxWidth: .infinity)
            .background(Color.brandPrimary)
            .foregroundColor(.white)
            .cornerRadius(27.5)
    }
}

struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedButton(text: "TEST")
    }
}
