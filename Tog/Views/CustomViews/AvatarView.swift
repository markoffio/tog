//
//  AvatarView.swift
//  Tog
//
//  Created by Marco Margarucci on 24/08/21.
//

import SwiftUI

struct AvatarView: View {
    // Size
    var size: CGFloat
    // Image
    var image: UIImage
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .scaledToFit()
            .frame(width: size, height: size)
            .clipShape(Circle())
            .foregroundColor(.brandSecondary)
    }
}

/*
struct AvatarView_Previews: PreviewProvider {
    static var previews: some View {
        AvatarView()
    }
}
*/
